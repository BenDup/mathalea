import ProportiondeProportion from '../techno1/techno1P4.js'
export const titre = 'Travailler sur des situations mettant en jeu des pourcentages de pourcentages'
export { interactifReady, interactifType } from '../techno1/techno1P4.js'
export const dateDePublication = '10/05/2023'
/**
 * Clone de techno1P4
 *
 * @author Stéphane Guyon cloné par Gilles Mora
 */
export const uuid = '0f1d1'
export const ref = '2S10-3'
export default function ProportiondeProportionS () {
  ProportiondeProportion.call(this)
}
