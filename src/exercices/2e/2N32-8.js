import CalculValeurApprocheeRacineCarree from '../4e/4G20-6.js'
export const titre = 'Encadrer une racine carrée'
export { interactifReady, interactifType } from '../4e/4G20-6.js'
export const dateDePublication = '09/05/2023'
/**
 * Clone de 4G20-6
 *
 * @author Guillaume Valmont  (Amélioration AMC par Eric Elter) cloné par Gilles Mora
 */
export const uuid = '99c84'
export const ref = '2N32-8'
export default function CalculValeurApprocheeRacineCarreeS () {
  CalculValeurApprocheeRacineCarree.call(this)
}
