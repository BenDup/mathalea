import { lettreDepuisChiffre, sp } from '../../lib/outils/outilString.js'
import Exercice from '../Exercice.js'
import choisirExpressionNumerique from './_choisirExpressionNumerique.js'
import ChoisirExpressionLitterale from './_Choisir_expression_litterale.js'
import {
  listeQuestionsToContenu,
  randint,
  gestionnaireFormulaireTexte
} from '../../modules/outils.js'
import { setReponse } from '../../lib/interactif/gestionInteractif.js'
import { ajouteChampTexteMathLive } from '../../lib/interactif/questionMathLive.js'
import { context } from '../../modules/context.js'
import { miseEnEvidence, texteEnCouleurEtGras } from '../../lib/outils/embellissements.js'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCOpenNum'
/**
* Fonction noyau pour 6 fonctions qui utilisent les mêmes variables et la fonction choisirExpressionNumerique
* @author Jean-Claude Lhote
* Référence 5C11, 5C11-1, 5C12-1, 5L10-1, 5L10-3, 5L14-1 et 5L14-3
*/
export default function EcrireUneExpressionNumerique (calculMental) {
  Exercice.call(this) // Héritage de la classe Exercice()
  this.consigne = ''
  this.nbQuestions = 4
  this.nbCols = 1
  this.nbColsCorr = 1
  this.sup2 = false // si false alors utilisation de nombres entiers, si true alors utilisation de nombres à un chiffre après la virgule.
  this.sup = 6
  this.sup3 = true
  this.version = 1 // 1 pour ecrire une expression, 2 pour écrire la phrase, 3 pour écrire l'expression et la calculer, 4 pour calculer une expression numérique

  this.nouvelleVersion = function () {
    this.autoCorrection = []
    let reponse
    this.listeQuestions = [] // Liste de questions
    this.listeCorrections = [] // Liste de questions corrigées
    let listeTypeDeQuestions
    if (typeof this.sup4 === 'number' && isNaN(this.sup)) {
      if (this.sup4 === 1) {
        this.sup = '1-1-2-2-3'
      } else if (this.sup4 === 2) {
        this.sup = '2-2-3-3-3'
      } else if (this.sup4 === 3) {
        this.sup = '2-2-3-3-4'
      } else if (this.sup4 === 4) {
        this.sup = '2-3-4-5'
      }
      listeTypeDeQuestions = gestionnaireFormulaireTexte({ saisie: this.sup, min: 1, max: 5, melange: 6, defaut: 6, nbQuestions: this.nbQuestions, shuffle: true })
    } else {
      listeTypeDeQuestions = gestionnaireFormulaireTexte({ saisie: this.sup, min: 2, max: 5, melange: 6, defaut: 6, nbQuestions: this.nbQuestions, shuffle: true })
    }
    let expf; let expn; let expc; let decimal; let nbval; let nbOperations; let resultats
    if (!calculMental) {
      decimal = 10
    } else {
      decimal = 1
    }
    // pour 5C12-1
    if (this.sup2) {
      decimal = 10
    } else {
      decimal = 1
    }
    for (let i = 0, texte, texteCorr, val1, val2, cpt = 0; i < this.nbQuestions && cpt < 50;) {
      this.autoCorrection[i] = {}
      nbOperations = parseInt(listeTypeDeQuestions[i] % 6)
      val1 = randint(2, 5)
      val2 = randint(6, 9)
      if (this.version > 2 && nbOperations === 1 && !this.litteral) nbOperations++
      if (!this.litteral) { resultats = choisirExpressionNumerique(nbOperations, decimal, this.sup3, calculMental) } else { resultats = ChoisirExpressionLitterale(nbOperations, decimal, val1, val2, this.sup3, calculMental) }
      expf = resultats[0]
      expn = resultats[1].split('=')[0]
      expn += expn[expn.length - 1] !== '$' ? '$' : ''
      expc = resultats[2]
      nbval = resultats[3]
      switch (this.version) {
        case 1:
          this.consigne = 'Traduire la phrase par un calcul (il n\'est pas demandé d\'effectuer ce calcul).'
          texte = `${expf}.`
          expn = expn.split(' ou ') // Pour traiter le cas du 'ou'.
          texteCorr = `${expf} s'écrit : $${miseEnEvidence(expn[0].substring(1,expn[0].length-1))}$`
          texteCorr += expn.length>1 ? ` ou $${miseEnEvidence(expn[1].substring(1,expn[1].length-1))}$.` :'.'
          break
        case 2:
          if (expn.indexOf('ou') > 0) expn = expn.substring(0, expn.indexOf('ou') - 1) // on supprime la deuxième expression fractionnaire
          this.consigne = 'Traduire le calcul par une phrase en français.'
          texte = `${expn}`
          expf = 'l' + expf.substring(1)
          texteCorr = `${expn} s'écrit : ${texteEnCouleurEtGras(expf)}.`
          break
        case 3:
          if (this.interactif) {
            this.consigne = 'Traduire la phrase par un calcul et effectuer le calcul demandé au brouillon.<br> Saisir uniquement le résultat.'
          } else {
            this.consigne = 'Traduire la phrase par un calcul et effectuer le calcul demandé.'
          }
          if (!this.litteral) texte = `${expf}.`
          else if (nbval === 2) texte = `${expf} puis calculer pour $x=${val1}$ et $y=${val2}$.` // nbval contient le nombre de valeurs en cas de calcul littéral
          else texte = `${expf} puis calculer pour $x=${val1}$.`
          texteCorr = `${expf} s'écrit : ${resultats[1]}.<br>`

          if (!this.litteral) {
            texteCorr = ''
              if (!this.sup4) {
              // texteCorr = `${expc}`
              const expc2=expc.substring(1,expc.length-1).split('=')
              texteCorr+=`$${miseEnEvidence(expc2[0])} =$`+ sp()
              for (let ee=1;ee<expc2.length-1;ee++) {
                texteCorr+=`$${expc2[ee]}  = $`+ sp()
              }
              texteCorr+=`$${miseEnEvidence(expc2[expc2.length-1])}$`
              
             } else {
              // On découpe
              const etapes = expc.split('=')
              let nbEtapes = 0
              etapes.forEach(function (etape) {
                nbEtapes++
                etape = etape.replace('$', '')
                if (context.isHtml) {
                  texteCorr += '<br>'
                }
                texteCorr += (nbEtapes=== etapes.length || nbEtapes===1) ? `${texteEnCouleurEtGras(lettreDepuisChiffre(i + 1)+' = ')} $${miseEnEvidence(etape)}$ <br>` : `${lettreDepuisChiffre(i + 1)} = $${etape}$ <br>`
              })
            }
          } else if (nbval === 2) texteCorr += `Pour $x=${val1}$ et $y=${val2}$ :<br> ${expc}`
          else texteCorr += `Pour $x=${val1}$ :<br>${expc}`
          // reponse = parseInt(expc.split('=')[expc.split('=').length - 1])
          reponse = expc.split('=')[expc.split('=').length - 1].replace('$', '')
          break
        case 4:
          if (expn.indexOf('ou') > 0) expn = expn.substring(0, expn.indexOf('ou') - 1) // on supprime la deuxième expression fractionnaire
          this.consigne = ''
          if (!this.litteral) texte = `${expn}`
          else if (nbval === 2) texte = `Pour $x=${val1}$ et $y=${val2}$, calculer ${expn}.`
          else texte = `Pour $x=${val1}$, calculer ${expn}.`
          if (!this.litteral) texteCorr = `${expc}`
          else if (nbval === 2) texteCorr = `Pour $x=${val1}$ et $y=${val2}$ :<br>${expc}`
          else texteCorr = `Pour $x=${val1}$ :<br>${expc}`
          reponse = parseInt(expc.split('=')[expc.split('=').length - 1])
          break
      }
      if ((this.questionJamaisPosee(i, nbOperations, nbval, this.version) && !this.litteral) || (this.litteral && this.questionJamaisPosee(i, nbOperations, nbval, this.version, resultats[4]))) { // Si la question n'a jamais été posée, on en créé une autre
        if (this.version > 2) {
          if (!context.isAmc) {
            texte += '<br>' + ajouteChampTexteMathLive(this, i, 'largeur25 inline', { texte: ' Résultat : ' })
          } else {
            texte += '<br>Détailler les calculs dans le cadre et coder le résultat.<br>'
          }
          setReponse(this, i, reponse)
        }
        this.listeQuestions.push(texte)
        this.listeCorrections.push(texteCorr)
        i++
      }
      cpt++
    }
    listeQuestionsToContenu(this)
  }
}
