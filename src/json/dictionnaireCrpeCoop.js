export const dictionnaireCrpeCoop = {
  'crpe-2022-g1-ex1': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 1',
    numeroInitial: 'Ex 1',
    png: [
      'static/crpe/2022/2022-g1-ex1.png'
    ],
    pngCor: [
      'static/crpe/2022/2022-g1-ex1-corr.png'
    ],
    url: [
      'static/crpe/2022/2022-g1-ex1.tex'
    ],
    urlcor: [
      'static/crpe/2022/2022-g1-ex1-corr.tex'
    ],
    tags: ['Vitesses', 'Nombre π', 'Durées', 'Tableur']

  },
  'crpe-2022-g1-ex2': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 1',
    numeroInitial: 'Ex 2',
    png: [
      'static/crpe/2022/2022-g1-ex2.png'
    ],
    pngCor: [
      'static/crpe/2022/2022-g1-ex2-corr.png'
    ],
    url: [
      'static/crpe/2022/2022-g1-ex2.tex'
    ],
    urlcor: [
      'static/crpe/2022/2022-g1-ex2-corr.tex'
    ],
    tags: ['Probabilités']
  },
  'crpe-2022-g1-ex3': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 1',
    numeroInitial: 'Ex 3',
    png: [
      'static/crpe/2022/2022-g1-ex3.png'
    ],
    pngCor: [
      'static/crpe/2022/2022-g1-ex3-corr.png'
    ],
    url: [
      'static/crpe/2022/2022-g1-ex3.tex'
    ],
    urlcor: [
      'static/crpe/2022/2022-g1-ex3-corr.tex'
    ],
    tags: ['Résolution de problèmes', 'Équations']
  },
  'crpe-2022-g1-ex4': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 1',
    numeroInitial: 'Ex 4',
    png: [
      'static/crpe/2022/2022-g1-ex4.png'
    ],
    pngCor: [
      'static/crpe/2022/2022-g1-ex4-corr.png'
    ],
    url: [
      'static/crpe/2022/2022-g1-ex4.tex'
    ],
    urlcor: [
      'static/crpe/2022/2022-g1-ex4-corr.tex'
    ],
    tags: ['Volumes', 'Aires latérales', 'Pythagore', 'Agrandissement-réduction', 'Fonctions', 'Inéquations']
  },
  'crpe-2022-g1-ex5': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 1',
    numeroInitial: 'Ex 5',
    png: [
      'static/crpe/2022/2022-g1-ex5.png'
    ],
    pngCor: [
      'static/crpe/2022/2022-g1-ex5-corr.png'
    ],
    url: [
      'static/crpe/2022/2022-g1-ex5.tex'
    ],
    urlcor: [
      'static/crpe/2022/2022-g1-ex5-corr.tex'
    ],
    tags: ['Volumes', 'Pythagore', 'Agrandissement-réduction', 'Fonctions', 'Inéquations']
  },
  'crpe-2022-g2-ex1': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 2',
    numeroInitial: 'Ex 1',
    png: [
      'static/crpe/2022/2022-g2-ex1.png'
    ],
    pngCor: [
      'static/crpe/2022/2022-g2-ex1-corr.png'
    ],
    url: [
      'static/crpe/2022/2022-g2-ex1.tex'
    ],
    urlcor: [
      'static/crpe/2022/2022-g2-ex1-corr.tex'
    ],
    tags: ['Probabilités']
  },
  'crpe-2022-g2-ex2': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 2',
    numeroInitial: 'Ex 2',
    png: [
      'static/crpe/2022/2022-g2-ex2.png'
    ],
    pngCor: [
      'static/crpe/2022/2022-g2-ex2-corr.png'
    ],
    url: [
      'static/crpe/2022/2022-g2-ex2.tex'
    ],
    urlcor: [
      'static/crpe/2022/2022-g2-ex2-corr.tex'
    ],
    tags: ['Vitesses', 'Agrandissement-réduction']
  },
  'crpe-2022-g2-ex3': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 2',
    numeroInitial: 'Ex 3',
    png: [
      'static/crpe/2022/2022-g2-ex3.png'
    ],
    pngCor: [
      'static/crpe/2022/2022-g2-ex3-corr.png'
    ],
    url: [
      'static/crpe/2022/2022-g2-ex3.tex'
    ],
    urlcor: [
      'static/crpe/2022/2022-g2-ex3-corr.tex'
    ],
    tags: ['Géométrie plane', 'Pythagore', 'Volumes', 'Agrandissement-réduction', 'Équations', 'Fonctions', 'Inéquations']
  },
  'crpe-2022-g2-ex4': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 2',
    numeroInitial: 'Ex 4',
    png: [
      'static/crpe/2022/2022-g2-ex4.png'
    ],
    pngCor: [
      'static/crpe/2022/2022-g2-ex4-corr.png'
    ],
    url: [
      'static/crpe/2022/2022-g2-ex4.tex'
    ],
    urlcor: [
      'static/crpe/2022/2022-g2-ex4-corr.tex'
    ],
    tags: ['Programmes de calculs', 'Calcul littéral', 'Équations']
  },
  'crpe-2022-g2-ex5': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 2',
    numeroInitial: 'Ex 5',
    png: [
      'static/crpe/2022/2022-g2-ex5.png'
    ],
    pngCor: [
      'static/crpe/2022/2022-g2-ex5-corr.png'
    ],
    url: [
      'static/crpe/2022/2022-g2-ex5.tex'
    ],
    urlcor: [
      'static/crpe/2022/2022-g2-ex5-corr.tex'
    ],
    tags: ['Numération']
  },
  'crpe-2022-g3-ex1': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 3',
    numeroInitial: 'Ex 1',
    png: [
      'static/crpe/2022/2022-g3-ex1.png'
    ],
    pngCor: [
      'static/crpe/2022/2022-g3-ex1-corr.png'
    ],
    url: [
      'static/crpe/2022/2022-g3-ex1.tex'
    ],
    urlcor: [
      'static/crpe/2022/2022-g3-ex1-corr.tex'
    ],
    tags: ['QCM', 'Volumes', 'Puissances', 'Pourcentages', 'Fractions', 'Pythagore']
  },
  'crpe-2022-g3-ex2': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 3',
    numeroInitial: 'Ex 2',
    png: [
      'static/crpe/2022/2022-g3-ex2.png'
    ],
    pngCor: [
      'static/crpe/2022/2022-g3-ex2-corr.png'
    ],
    url: [
      'static/crpe/2022/2022-g3-ex2.tex'
    ],
    urlcor: [
      'static/crpe/2022/2022-g3-ex2-corr.tex'
    ],
    tags: ['Statistiques', 'Agrandissement-réduction', 'Nombre π', 'Durées', 'Vitesses', 'Proportionnalité']
  },
  'crpe-2022-g3-ex3': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 3',
    numeroInitial: 'Ex 3',
    png: [
      'static/crpe/2022/2022-g3-ex3.png'
    ],
    pngCor: [
      'static/crpe/2022/2022-g3-ex3-corr.png'
    ],
    url: [
      'static/crpe/2022/2022-g3-ex3.tex'
    ],
    urlcor: [
      'static/crpe/2022/2022-g3-ex3-corr.tex'
    ],
    tags: ['Pythagore', 'Thalès', 'Calcul littéral', 'Lecture graphique', 'Volumes', 'Pourcentages', 'Proportionnalité']
  },
  'crpe-2022-g3-ex4': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 3',
    numeroInitial: 'Ex 4',
    png: [
      'static/crpe/2022/2022-g3-ex4.png'
    ],
    pngCor: [
      'static/crpe/2022/2022-g3-ex4-corr.png'
    ],
    url: [
      'static/crpe/2022/2022-g3-ex4.tex'
    ],
    urlcor: [
      'static/crpe/2022/2022-g3-ex4-corr.tex'
    ],
    tags: ['Algorithmique et programmation']
  },
  'crpe-2023-g1-ex1': {
    typeExercice: 'crpe',
    annee: '2023',
    lieu: 'Groupement 1',
    numeroInitial: 'Ex 1',
    png: [
      'static/crpe/2023/2023-g1-ex1.png'
    ],
    pngCor: [
      'static/crpe/2023/2023-g1-ex1-corr.png'
    ],
    url: [
      'static/crpe/2023/2023-g1-ex1.tex'
    ],
    urlcor: [
      'static/crpe/2023/2023-g1-ex1-corr.tex'
    ],
    tags: ['Pythagore', 'Vitesses']
  },
  'crpe-2023-g1-ex2': {
    typeExercice: 'crpe',
    annee: '2023',
    lieu: 'Groupement 1',
    numeroInitial: 'Ex 2',
    png: [
      'static/crpe/2023/2023-g1-ex2.png'
    ],
    pngCor: [
      'static/crpe/2023/2023-g1-ex2-corr.png'
    ],
    url: [
      'static/crpe/2023/2023-g1-ex2.tex'
    ],
    urlcor: [
      'static/crpe/2023/2023-g1-ex2-corr.tex'
    ],
    tags: ['Résolution de problèmes', 'Fractions']
  },
  'crpe-2023-g1-ex3': {
    typeExercice: 'crpe',
    annee: '2023',
    lieu: 'Groupement 1',
    numeroInitial: 'Ex 3',
    png: [
      'static/crpe/2023/2023-g1-ex3.png'
    ],
    pngCor: [
      'static/crpe/2023/2023-g1-ex3-corr.png'
    ],
    url: [
      'static/crpe/2023/2023-g1-ex3.tex'
    ],
    urlcor: [
      'static/crpe/2023/2023-g1-ex3-corr.tex'
    ],
    tags: ['Algorithmique et programmation']
  },
  'crpe-2023-g1-ex4': {
    typeExercice: 'crpe',
    annee: '2023',
    lieu: 'Groupement 1',
    numeroInitial: 'Ex 4',
    png: [
      'static/crpe/2023/2023-g1-ex4.png'
    ],
    pngCor: [
      'static/crpe/2023/2023-g1-ex4-corr.png'
    ],
    url: [
      'static/crpe/2023/2023-g1-ex4.tex'
    ],
    urlcor: [
      'static/crpe/2023/2023-g1-ex4-corr.tex'
    ],
    tags: ['Proportionnalité', 'Pythagore', 'Aires', 'Ratio', 'Volumes']
  },
  'crpe-2023-g1-ex5': {
    typeExercice: 'crpe',
    annee: '2023',
    lieu: 'Groupement 1',
    numeroInitial: 'Ex 5',
    png: [
      'static/crpe/2023/2023-g1-ex5.png'
    ],
    pngCor: [
      'static/crpe/2023/2023-g1-ex5-corr.png'
    ],
    url: [
      'static/crpe/2023/2023-g1-ex5.tex'
    ],
    urlcor: [
      'static/crpe/2023/2023-g1-ex5-corr.tex'
    ],
    tags: ['Proportionnalité', 'Résolution de problèmes', 'Tableur']
  },
  'crpe-2023-g1-ex6': {
    typeExercice: 'crpe',
    annee: '2023',
    lieu: 'Groupement 1',
    numeroInitial: 'Ex 6',
    png: [
      'static/crpe/2023/2023-g1-ex6.png'
    ],
    pngCor: [
      'static/crpe/2023/2023-g1-ex6-corr.png'
    ],
    url: [
      'static/crpe/2023/2023-g1-ex6.tex'
    ],
    urlcor: [
      'static/crpe/2023/2023-g1-ex6-corr.tex'
    ],
    tags: ['Probabilités', 'Pourcentages']
  },
  'crpe-2023-g2-ex1': {
    typeExercice: 'crpe',
    annee: '2023',
    lieu: 'Groupement 2',
    numeroInitial: 'Ex 1',
    png: [
      'static/crpe/2023/2023-g2-ex1.png'
    ],
    pngCor: [
      'static/crpe/2023/2023-g2-ex1-corr.png'
    ],
    url: [
      'static/crpe/2023/2023-g2-ex1.tex'
    ],
    urlcor: [
      'static/crpe/2023/2023-g2-ex1-corr.tex'
    ],
    tags: ['Pythagore', 'Périmètres', 'Proportionnalité', 'Statistiques', 'Pourcentages']
  },
  'crpe-2023-g2-ex2': {
    typeExercice: 'crpe',
    annee: '2023',
    lieu: 'Groupement 2',
    numeroInitial: 'Ex 2',
    png: [
      'static/crpe/2023/2023-g2-ex2.png'
    ],
    pngCor: [
      'static/crpe/2023/2023-g2-ex2-corr.png'
    ],
    url: [
      'static/crpe/2023/2023-g2-ex2.tex'
    ],
    urlcor: [
      'static/crpe/2023/2023-g2-ex2-corr.tex'
    ],
    tags: ['Propriétés des polygones']
  },
  'crpe-2023-g2-ex3': {
    typeExercice: 'crpe',
    annee: '2023',
    lieu: 'Groupement 2',
    numeroInitial: 'Ex 3',
    png: [
      'static/crpe/2023/2023-g2-ex3.png'
    ],
    pngCor: [
      'static/crpe/2023/2023-g2-ex3-corr.png'
    ],
    url: [
      'static/crpe/2023/2023-g2-ex3.tex'
    ],
    urlcor: [
      'static/crpe/2023/2023-g2-ex3-corr.tex'
    ],
    tags: ['Algorithmique et programmation', 'Programmes de calculs', 'Calcul littéral']
  },
  'crpe-2023-g2-ex4': {
    typeExercice: 'crpe',
    annee: '2023',
    lieu: 'Groupement 2',
    numeroInitial: 'Ex 4',
    png: [
      'static/crpe/2023/2023-g2-ex4.png'
    ],
    pngCor: [
      'static/crpe/2023/2023-g2-ex4-corr.png'
    ],
    url: [
      'static/crpe/2023/2023-g2-ex4.tex'
    ],
    urlcor: [
      'static/crpe/2023/2023-g2-ex4-corr.tex'
    ],
    tags: ['Probabilités']
  },
  'crpe-2023-g2-ex5': {
    typeExercice: 'crpe',
    annee: '2023',
    lieu: 'Groupement 2',
    numeroInitial: 'Ex 5',
    png: [
      'static/crpe/2023/2023-g2-ex5.png'
    ],
    pngCor: [
      'static/crpe/2023/2023-g2-ex5-corr.png'
    ],
    url: [
      'static/crpe/2023/2023-g2-ex5.tex'
    ],
    urlcor: [
      'static/crpe/2023/2023-g2-ex5-corr.tex'
    ],
    tags: ['Calcul littéral']
  },
  'crpe-2023-g2-ex6': {
    typeExercice: 'crpe',
    annee: '2023',
    lieu: 'Groupement 2',
    numeroInitial: 'Ex 6',
    png: [
      'static/crpe/2023/2023-g2-ex6.png'
    ],
    pngCor: [
      'static/crpe/2023/2023-g2-ex6-corr.png'
    ],
    url: [
      'static/crpe/2023/2023-g2-ex6.tex'
    ],
    urlcor: [
      'static/crpe/2023/2023-g2-ex6-corr.tex'
    ],
    tags: ['Calcul littéral']
  },
  'crpe-2023-g3-ex1': {
    typeExercice: 'crpe',
    annee: '2023',
    lieu: 'Groupement 3',
    numeroInitial: 'Ex 1',
    png: [
      'static/crpe/2023/2023-g3-ex1.png'
    ],
    pngCor: [
      'static/crpe/2023/2023-g3-ex1-corr.png'
    ],
    url: [
      'static/crpe/2023/2023-g3-ex1.tex'
    ],
    urlcor: [
      'static/crpe/2023/2023-g3-ex1-corr.tex'
    ],
    tags: ['Arithmétique', 'Tableur']
  },
  'crpe-2023-g3-ex2': {
    typeExercice: 'crpe',
    annee: '2023',
    lieu: 'Groupement 3',
    numeroInitial: 'Ex 2',
    png: [
      'static/crpe/2023/2023-g3-ex2.png'
    ],
    pngCor: [
      'static/crpe/2023/2023-g3-ex2-corr.png'
    ],
    url: [
      'static/crpe/2023/2023-g3-ex2.tex'
    ],
    urlcor: [
      'static/crpe/2023/2023-g3-ex2-corr.tex'
    ],
    tags: ['Pythagore', 'Thalès', 'Lecture graphique', 'Proportionnalité']
  },
  'crpe-2023-g3-ex3': {
    typeExercice: 'crpe',
    annee: '2023',
    lieu: 'Groupement 3',
    numeroInitial: 'Ex 3',
    png: [
      'static/crpe/2023/2023-g3-ex3.png'
    ],
    pngCor: [
      'static/crpe/2023/2023-g3-ex3-corr.png'
    ],
    url: [
      'static/crpe/2023/2023-g3-ex3.tex'
    ],
    urlcor: [
      'static/crpe/2023/2023-g3-ex3-corr.tex'
    ],
    tags: ['Algorithmique et programmation', 'Propriétés des polygones', 'Proportionnalité']
  },
  'crpe-2023-g3-ex4': {
    typeExercice: 'crpe',
    annee: '2023',
    lieu: 'Groupement 3',
    numeroInitial: 'Ex 4',
    png: [
      'static/crpe/2023/2023-g3-ex4.png'
    ],
    pngCor: [
      'static/crpe/2023/2023-g3-ex4-corr.png'
    ],
    url: [
      'static/crpe/2023/2023-g3-ex4.tex'
    ],
    urlcor: [
      'static/crpe/2023/2023-g3-ex4-corr.tex'
    ],
    tags: ['Probabilités']
  },
  'crpe-2023-g3-ex5': {
    typeExercice: 'crpe',
    annee: '2023',
    lieu: 'Groupement 3',
    numeroInitial: 'Ex 5',
    png: [
      'static/crpe/2023/2023-g3-ex5.png'
    ],
    pngCor: [
      'static/crpe/2023/2023-g3-ex5-corr.png'
    ],
    url: [
      'static/crpe/2023/2023-g3-ex5.tex'
    ],
    urlcor: [
      'static/crpe/2023/2023-g3-ex5-corr.tex'
    ],
    tags: ['Vrai-faux', 'Ensembles de nombres', 'Calcul littéral', 'Pourcentages', 'Pythagore', 'Propriétés des polygones', 'Fonctions', 'Agrandissement-réduction']
  }
}

export const garderPourTagsEx4 =
{
  'crpe-2022-g1-ex1': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 1',
    numeroInitial: 'Ex 1',
    png: [
      'static/crpe/2022/2022-g1-ex1.tex'
    ],
    pngCor: [
      'static/crpe/2022/2022-g1-ex1-corr.tex'
    ]
  },
  'crpe-2022-g1-ex2': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 1',
    numeroInitial: 'Ex 2',
    png: [
      'static/crpe/2022/2022-g1-ex2.tex'
    ],
    pngCor: [
      'static/crpe/2022/2022-g1-ex2-corr.tex'
    ],
    tags: []
  },
  'crpe-2022-g1-ex3': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 1',
    numeroInitial: 'Ex 3',
    png: [
      'static/crpe/2022/2022-g1-ex3.tex'
    ],
    pngCor: [
      'static/crpe/2022/2022-g1-ex3-corr.tex'
    ],
    tags: []
  },
  'crpe-2022-g1-ex4': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 1',
    numeroInitial: 'Ex 4',
    png: [
      'static/crpe/2022/2022-g1-ex4.tex'
    ],
    pngCor: [
      'static/crpe/2022/2022-g1-ex4-corr.tex'
    ],
    tags: ['Algorithmique et programmation', 'Géométrie plane']
  },
  'crpe-2022-g1-ex5': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 1',
    numeroInitial: 'Ex 5',
    png: [
      'static/crpe/2022/2022-g1-ex5.tex'
    ],
    pngCor: [
      'static/crpe/2022/2022-g1-ex5-corr.tex'
    ],
    tags: []
  },
  'crpe-2022-g2-ex1': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 2',
    numeroInitial: 'Ex 1',
    png: [
      'static/crpe/2022/2022-g2-ex1.tex'
    ],
    pngCor: [
      'static/crpe/2022/2022-g2-ex1-corr.tex'
    ],
    tags: ['Probabilités']
  },
  'crpe-2022-g2-ex2': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 2',
    numeroInitial: 'Ex 2',
    png: [
      'static/crpe/2022/2022-g2-ex2.tex'
    ],
    pngCor: [
      'static/crpe/2022/2022-g2-ex2-corr.tex'
    ],
    tags: ['Vitesses', 'Agrandissement-réduction']
  },
  'crpe-2022-g2-ex3': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 2',
    numeroInitial: 'Ex 3',
    png: [
      'static/crpe/2022/2022-g2-ex3.tex'
    ],
    pngCor: [
      'static/crpe/2022/2022-g2-ex3-corr.tex'
    ],
    tags: ['Géométrie plane', 'Pythagore', 'Volumes', 'Agrandissement-réduction', 'Équations', 'Fonctions', 'Inéquations']
  },
  'crpe-2022-g2-ex4': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 2',
    numeroInitial: 'Ex 4',
    png: [
      'static/crpe/2022/2022-g2-ex4.tex'
    ],
    pngCor: [
      'static/crpe/2022/2022-g2-ex4-corr.tex'
    ],
    tags: ['Programmes de calculs', 'Calcul littéral', 'Équations']
  },
  'crpe-2022-g2-ex5': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 2',
    numeroInitial: 'Ex 5',
    png: [
      'static/crpe/2022/2022-g2-ex5.tex'
    ],
    pngCor: [
      'static/crpe/2022/2022-g2-ex5-corr.tex'
    ],
    tags: ['Numération']
  },
  'crpe-2022-g3-ex1': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 3',
    numeroInitial: 'Ex 1',
    png: [
      'static/crpe/2022/2022-g3-ex1.tex'
    ],
    pngCor: [
      'static/crpe/2022/2022-g3-ex1-corr.tex'
    ],
    tags: ['QCM', 'Volumes', 'Puissances', 'Pourcentages', 'Fractions', 'Pythagore']
  },
  'crpe-2022-g3-ex2': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 3',
    numeroInitial: 'Ex 2',
    png: [
      'static/crpe/2022/2022-g3-ex2.tex'
    ],
    pngCor: [
      'static/crpe/2022/2022-g3-ex2-corr.tex'
    ],
    tags: ['Statistiques', 'Agrandissement-réduction', 'Nombre π', 'Durées', 'Vitesses', 'Proportionnalité']
  },
  'crpe-2022-g3-ex3': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 3',
    numeroInitial: 'Ex 3',
    png: [
      'static/crpe/2022/2022-g3-ex3.tex'
    ],
    pngCor: [
      'static/crpe/2022/2022-g3-ex3-corr.tex'
    ],
    tags: ['Pythagore', 'Thalès', 'Calcul littéral', 'Lecture graphique', 'Volumes', 'Pourcentages', 'Proportionnalité']
  },
  'crpe-2022-g3-ex4': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 3',
    numeroInitial: 'Ex 4',
    png: [
      'static/crpe/2022/2022-g3-ex4.tex'
    ],
    pngCor: [
      'static/crpe/2022/2022-g3-ex4-corr.tex'
    ],
    tags: ['Algorithmique et programmation']
  },
  'crpe-2022-g4-ex1': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 4',
    numeroInitial: 'Ex 1',
    png: [
      'static/crpe/2022/2022-g4-ex1.tex'
    ],
    pngCor: [
      'static/crpe/2022/2022-g4-ex1-corr.tex'
    ],
    tags: ['Probabilités', 'Équations']
  },
  'crpe-2022-g4-ex2': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 4',
    numeroInitial: 'Ex 2',
    png: [
      'static/crpe/2022/2022-g4-ex2.tex'
    ],
    pngCor: [
      'static/crpe/2022/2022-g4-ex2-corr.tex'
    ],
    tags: ['Pourcentages', 'Proportionnalité', 'Fonctions', 'Volumes']
  },
  'crpe-2022-g4-ex3': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 4',
    numeroInitial: 'Ex 3',
    png: [
      'static/crpe/2022/2022-g4-ex3.tex'
    ],
    pngCor: [
      'static/crpe/2022/2022-g4-ex3-corr.tex'
    ],
    tags: ['Tableur', 'Résolution de problèmes', 'Équations']
  },
  'crpe-2022-g4-ex4': {
    typeExercice: 'crpe',
    annee: '2022',
    lieu: 'Groupement 4',
    numeroInitial: 'Ex 4',
    png: [
      'static/crpe/2022/2022-g4-ex4.tex'
    ],
    pngCor: [
      'static/crpe/2022/2022-g4-ex4-corr.tex'
    ],
    tags: ['Algorithmique et programmation']
  }
}
