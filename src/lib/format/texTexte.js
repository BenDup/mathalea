/**
 * Écrit du texte en mode mathématiques
 * @author Rémi Angot
 */
export function texTexte (texte) {
    return '~\\text{' + texte + '}'
  }